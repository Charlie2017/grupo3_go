import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.sound.sampled.AudioSystem;

public class ManejadorGo implements MouseListener {

    private int turno = 0;
    private TableroGo tableroGo;
    private VentanaGo ventanaGo;
    private Ficha ficha;
    private Tablero tablero;
    private ImageIcon fBlanca, fNegra;

    public ManejadorGo() {
        tablero = new Tablero();
        fBlanca = new ImageIcon("img/TABLERO-06.png");
        fNegra = new ImageIcon("img/TABLERO-07.png");
    }

    public void setTableroGo(TableroGo tableroGo) {
        this.tableroGo = tableroGo;
    }//actualiza el panel para mostrar la imagen

    public void prueba(Graphics g) {
        for (int fila = 0; fila < tablero.lengthFila(); fila++) {
            for (int columna = 0; columna < tablero.lengthColumna(); columna++) {

                Ficha fichaB = new Ficha(true, tablero.getX(columna), tablero.getY(fila), fBlanca);
                tablero.setFicha(fichaB, fila, columna);
            }
        }
        for (int fila = 0; fila < tablero.lengthFila(); fila++) {
            for (int columna = 0; columna < tablero.lengthColumna(); columna++) {
                if (tablero.getFicha(fila, columna) != null) {
                    tablero.getFicha(fila, columna).pintarFicha(g);
                } else {
                    System.out.println("");
                }
            }
        }
    }//fin prueba

    @Override
    public void mouseClicked(MouseEvent click) {
        System.out.println("Posicion X es: " + click.getX() + "\nPosicion Y es: " + click.getY());
        if (turno == 0 && tablero.evaluarPV(click.getX(), click.getY())) {
            for (int fila = 0; fila < tablero.lengthFila(); fila++) {
                for (int columna = 0; columna < tablero.lengthColumna(); columna++) {
                    if (click.getX() >= tablero.getX(columna) && click.getY() >= tablero.getY(fila)) {
                        if (tablero.generarHB(fila, columna, click.getX(), click.getY())) {
                            if (tablero.evaluarPosicionVacia(fila, columna)) {
                                    Ficha fichaNegra = new Ficha(false, click.getX(), click.getY(), fNegra);
                                    if (tablero.contarLib(fila, columna)) {
                                    tablero.setFicha(fichaNegra, fila, columna);
                                    Sonido prueba = new Sonido("birdo-spit.wav");
                                    prueba.sonar();
                                    System.out.println("Se puso la ficha");
                                    turno = 1;
                                }
                            }//fin evaluar posicion vacia
                            else {
                                JOptionPane.showMessageDialog(null, "esta posicion esta llena");
                            }//fin del else evaluar posicion vacia
                        }//fin del if, posiciona la ficha en el rango de la hitbox establecida
                    }//fin del if, comparar click con los arreglos
                }//fin del for columna

            }//fin del for fila

        }//fin del if
        else {
            for (int fila = 0; fila < tablero.lengthFila(); fila++) {
                for (int columna = 0; columna < tablero.lengthColumna(); columna++) {
                    if (click.getX() >= tablero.getX(columna) && click.getY() >= tablero.getY(fila)) {
                        if (tablero.generarHB(fila, columna, click.getX(), click.getY())) {
                            if (tablero.evaluarPosicionVacia(fila, columna)) {
                                    Ficha fichaBlanca = new Ficha(true, click.getX(), click.getY(), fBlanca);
                                    if (tablero.contarLib(fila, columna)) {
                                    tablero.setFicha(fichaBlanca, fila, columna);
                                    Sonido prueba = new Sonido("birdo-spit.wav");
                                    prueba.sonar();
                                    System.out.println("Se puso la ficha");
                                    turno = 0;
                                }
                            }//fin evaluarposicion vacia
                            else {
                                JOptionPane.showMessageDialog(null, "esta posicion esta llena");
                            }//fin del else evaluar posicion vacia
                        }//fin del if, posiciona la ficha en el rango de la hitbox establecida
                    }//fin del if, comparar click con los arreglos
                }//fin del for columna

            }//fin del for fila   

        }//fin del else
        tableroGo.repaint();
    }//fin del mouseClicked

    @Override
    public void mousePressed(MouseEvent arg0) {

    }

    @Override
    public void mouseReleased(MouseEvent arg0) {

    }

    @Override
    public void mouseEntered(MouseEvent arg0) {

    }

    @Override
    public void mouseExited(MouseEvent arg0) {

    }

    public void actualizar(Graphics g) {
        for (int fila = 0; fila < tablero.lengthFila(); fila++) {
            for (int columna = 0; columna < tablero.lengthColumna(); columna++) {
                if (tablero.getFicha(fila, columna) != null) {
                    tablero.getFicha(fila, columna).pintarFicha(g);
                } else {
                    System.out.println("");
                }
            }//fin for columna

        }//fin for fila
    }//pinta la ficha

}
