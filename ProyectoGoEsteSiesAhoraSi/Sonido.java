import java.io.File;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;


public class Sonido {

    private Clip sonido;

    //Contructor por defecto
    public Sonido() {
        this.sonar();
    }

    public Sonido(String archivo) {
        try {
            // Se obtiene un Clip de sonido
            sonido = AudioSystem.getClip();

            // Se carga con un fichero wav
            sonido.open(AudioSystem.getAudioInputStream(new File(archivo)));

        } catch (Exception e) {
            System.out.println("" + e);
        }
    }

    public void sonar() {
        // El sonido inicia
        sonido.setFramePosition(0);
        //Activar sonidos
        sonido.start();
    }

    public void parar() {
        // Detiene le sonido donde este sonando
        sonido.stop();
    }

    public void loop() {
        // Comienza la reproducción continua
        sonido.loop(Clip.LOOP_CONTINUOUSLY);
    }
}
