import javax.swing.JPanel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Graphics;
import java.awt.Color;

public class TableroGo extends JPanel{
	private Icon fondoTablero;
	private ManejadorGo manejadorGo;
	
	public TableroGo(ManejadorGo manejadorGo){
			super();
			this.manejadorGo=manejadorGo;
			fondoTablero=new ImageIcon("img/TABLERO-01.png");
			setLayout(null);
			addMouseListener(manejadorGo);
	}	
	
	public void paint(Graphics g){
		super.paintComponent(g);
		fondoTablero.paintIcon(null, g, 0, 0);
		manejadorGo.actualizar(g);
	}

}
