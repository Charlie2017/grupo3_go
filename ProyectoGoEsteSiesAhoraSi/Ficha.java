import java.awt.Graphics;
import javax.swing.Icon;


public class Ficha {
private boolean color;
private int x,y;
private Icon imagen;

    public Ficha(boolean color, int x, int y, Icon imagen) {
        this.color = color;
        this.x = x;
        this.y = y;
        this.imagen = imagen;
    }

    public boolean isColor() {
        return color;
    }

    public void setColor(boolean color) {
        this.color = color;
    }
    
    public boolean getColor(){
       return color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Icon getImagen() {
        return imagen;
    }

    public void setImagen(Icon imagen) {
        this.imagen = imagen;
    }
    
    public void pintarFicha(Graphics g){
    imagen.paintIcon(null, g, x, y);
    }
    
}
