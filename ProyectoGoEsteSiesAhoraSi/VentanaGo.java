import javax.swing.JFrame;

public class VentanaGo extends JFrame{
	private TableroGo tableroGo;
	private ManejadorGo manejadorGo;
	
	public VentanaGo(){
	super("Tablero");
	manejadorGo=new ManejadorGo();
	tableroGo=new TableroGo(manejadorGo);
	manejadorGo.setTableroGo(tableroGo);
	getContentPane().add(tableroGo);
	setSize(610,639);
	setVisible(true);
	setLocationRelativeTo(null);
	}	
	
}
