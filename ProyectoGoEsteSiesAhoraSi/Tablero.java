
public class Tablero {

    private Ficha tablero[][];
    private int posicionX[] = {11, 76, 141, 206, 271, 336, 401, 466, 531};//rangos de x
    private int posicionY[] = {4, 69, 134, 199, 264, 329, 394, 459, 524};//rangos de y
    private Ficha ficha;

    public Tablero() {
        tablero = new Ficha[9][9];
    }//genera la matriz

    public void setFicha(Ficha ficha, int fila, int columna) {
        ficha.setX(posicionX[columna]);
        ficha.setY(posicionY[fila]);
        tablero[fila][columna] = ficha;
    }//asignar una ficha creada a la matriz

    public Ficha getFicha(int fila, int columna) {
        return tablero[fila][columna];
    }//obtiene la ficha de la posicion clickeada que luego será pintada;

    public int lengthFila() {
        return tablero.length;
    }//determina el tamaño de la fila(no se pone el [])

    public int lengthColumna() {
        return tablero[0].length;
    }//determina el tamaño de la Columna

    public boolean generarHB(int fila, int columna, int x, int y) {
        if (x >= posicionX[columna] && x <= posicionX[columna] + 55 && y >= posicionY[fila] && y <= posicionY[fila] + 55) {
            return true;
        }
        return false;
    }//generar hitbox de las fichas

    public int getX(int columna) {
        return posicionX[columna];
    }//otiene dato que se encuentra en esa posicion de la columna en el arreglo

    public int getY(int fila) {
        return posicionY[fila];
    }//otiene dato que se encuentra en esa posicion de la fila en el arreglo

    public boolean evaluarPV(int x, int y) {
        for (int fila = 0; fila < tablero.length; fila++) {
            for (int columna = 0; columna < tablero.length; columna++) {
                if (generarHB(fila, columna, x, y)) {
                    return true;
                }//fin de la evaluacion
            }//fin del for columna
        }//fin del for fila 
        return false;
    }//evaluar posicion validad del click dentro del rango ya creado

    public boolean evaluarPosicionVacia(int fila, int columna) {
        if (tablero[fila][columna] == null) {
            return true;
        }
        return false;

    }

    public boolean contarLib( int fila, int columna) {
        if (fila == 0 && columna == 0) {
            if (!evaluarPosicionVacia(fila + 1, columna) && !evaluarPosicionVacia(fila, columna + 1)) {
                if (tablero[fila][columna].getColor() != tablero[fila + 1][columna].getColor() && tablero[fila][columna].getColor() != tablero[fila][columna + 1].getColor()) {
                    return false;
                } else {
                    return true;
                }
            }
        } else if (fila == 0 && columna == 8) {
            if (!evaluarPosicionVacia(fila + 1, columna) && !evaluarPosicionVacia(fila, columna - 1)) {
                if (tablero[fila][columna].getColor() != tablero[fila + 1][columna].getColor() && tablero[fila][columna].getColor() != tablero[fila][columna - 1].getColor()) {
                    return false;
                } else {
                    return true;
                }
            }
        } else if (fila == 8 && columna == 0) {
            if (!evaluarPosicionVacia(fila - 1, columna) && !evaluarPosicionVacia(fila, columna + 1)) {
                if (tablero[fila][columna].getColor() != tablero[fila - 8][columna].getColor() && tablero[fila][columna].getColor() != tablero[fila][columna + 1].getColor()) {
                    return false;
                } else {
                    return true;
                }
            }
        } else if (fila == 8 && columna == 8) {
            if (!evaluarPosicionVacia(fila - 1, columna) && !evaluarPosicionVacia(fila, columna - 1)) {
                if (tablero[fila][columna].getColor() != tablero[fila - 1][columna].getColor() && tablero[fila][columna].getColor() != tablero[fila][columna - 1].getColor()) {
                    return false;
                }
            } else {
                return true;
            }
        } else if (fila == 0) {
            if (!evaluarPosicionVacia(fila + 1, columna) && !evaluarPosicionVacia(fila, columna - 1) && !evaluarPosicionVacia(fila, columna + 1)) {
                if (tablero[fila][columna].getColor() != tablero[fila + 1][columna].getColor() && tablero[fila][columna].getColor() != tablero[fila][columna + 1].getColor() && tablero[fila][columna].getColor() != tablero[fila][columna - 1].getColor()) {
                    return false;
                } else {
                    return true;
                }
            }
        } else if (fila == 8) {
            if (!evaluarPosicionVacia(fila - 1, columna) && !evaluarPosicionVacia(fila, columna - 1) && !evaluarPosicionVacia(fila, columna + 1)) {
                if (tablero[fila][columna].getColor() != tablero[fila - 1][columna].getColor() && tablero[fila][columna].getColor() != tablero[fila][columna - 1].getColor() && tablero[fila][columna].getColor() != tablero[fila][columna + 1].getColor()) {
                    return false;
                } else {
                    return true;
                }
            }
        } else if (columna == 0) {
            if (!evaluarPosicionVacia(fila + 1, columna) && !evaluarPosicionVacia(fila - 1, columna) && !evaluarPosicionVacia(fila, columna + 1)) {
                if (tablero[fila][columna].getColor() != tablero[fila + 1][columna].getColor() && tablero[fila][columna].getColor() != tablero[fila - 1][columna].getColor() && tablero[fila][columna].getColor() != tablero[fila][columna + 1].getColor()) {
                    return false;
                } else {
                    return true;
                }
            }
        } else if (columna == 8) {
            if (!evaluarPosicionVacia(fila + 1, columna) && !evaluarPosicionVacia(fila - 1, columna) && !evaluarPosicionVacia(fila, columna - 1)) {
                if (tablero[fila][columna].getColor() != tablero[fila + 1][columna].getColor() && tablero[fila][columna].getColor() != tablero[fila - 1][columna].getColor() && tablero[fila][columna].getColor() != tablero[fila][columna - 1].getColor()) {
                    return false;
                } else {
                    return true;
                }
            }
        } else {
            if (!evaluarPosicionVacia(fila + 1, columna) && !evaluarPosicionVacia(fila - 1, columna) && !evaluarPosicionVacia(fila, columna + 1) && !evaluarPosicionVacia(fila, columna - 1)) {
                if (tablero[fila][columna].getColor() != tablero[fila + 1][columna].getColor() && tablero[fila][columna].getColor() != tablero[fila - 1][columna].getColor() && tablero[fila][columna].getColor() != tablero[fila][columna - 1].getColor() && tablero[fila][columna].getColor() != tablero[fila][columna + 1].getColor()) {
                    return false;
                } else {
                    return true;
                }
            }
        }
        return true;
    }

}//fin de la clase
